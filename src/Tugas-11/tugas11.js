import { render } from '@testing-library/react';
import React, {Component} from 'react';
import '../App.css';

class Tugas11 extends Component {
    constructor(props){
        super(props)
        this.state = {
          time: 100,
          timeNow: new Date("August 19, 1975 23:15:30 GMT+00:00"),
          showTime: true
        }
    }
    componentDidMount(){
        if (this.props.start !== undefined){
          this.setState({time: this.props.start})
        }
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
      }

      componentWillUnmount(){
        this.setState({showTime: false});
      }
    
      componentDidUpdate() {
        if (this.state.time === 0){
        this.componentWillUnmount()      
         }
      }
    
    
    tick() {
        this.setState({
          time: this.state.time - 1 
        });
      }
    

render() {
    return (<>
        {
            <h1 style={{textAlign: "center"}}>
            sekarang jam:    {this.state.timeNow.toLocaleTimeString('en-US')}
            <br />
            hitung mundur:   {this.state.time}
            </h1>
        } 
        </>
    )
}
}
export default Tugas11