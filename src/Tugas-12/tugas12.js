import React, {Component} from 'react';
import '../App.css';

class tugas12 extends Component{

    constructor(props){
      super(props)
      this.state ={
       namaBuah : [ 'Semangka', 'Anggur', 'Jeruk', 'Strawberry', 'Mangga' ],
       hargaBuah: [ 10000, 40000, 30000, 15000, 20000 ],
       beratBuah: [ 1000, 400, 300, 150, 200 ],
       inputHarga: "",
       inputBuah: "",
       inputBerat: "",
       index: -1  
      }
      // bind in constructor
      this.handleEdit = this.handleEdit.bind(this)
    }
  
    // method with normal function
    handleSubmit(event){
      event.preventDefault()
      let index = this.state.index
      let inputBuah = this.state.inputBuah
      let inputHarga = this.state.inputHarga
      let inputBerat = this.state.inputBerat
      let namaBuah = this.state.namaBuah
      let hargaBuah = this.state.hargaBuah
      let beratBuah = this.state.beratBuah
      // if index -1 means submit create
      if (index === -1){
        this.setState({namaBuah: [...namaBuah, inputBuah], inputBuah: ""})
        this.setState({hargaBuah: [...hargaBuah, inputHarga], inputHarga: ""})
        this.setState({beratBuah: [...beratBuah, inputBerat], inputBerat: ""})
      }else{
        // means submit edit
        namaBuah[index] = inputBuah
        this.setState({namaBuah, inputBuah: ""})
        hargaBuah[index] = inputHarga
        this.setState({hargaBuah, inputHarga: ""})
        beratBuah[index] = inputBerat
        this.setState({beratBuah, inputBerat: ""})
      }
    }
  
    handleEdit(event){
      let index = event.target.value;
      this.setState({inputBuah: this.state.namaBuah[index], index})
      this.setState({inputHarga: this.state.hargaBuah[index], index})
      this.setState({inputBerat: this.state.beratBuah[index], index})
  
    }
  
    //method with arrow function
    handleChange = (event) =>{
      var value = event.target.value
      this.setState({
        inputBuah: value,
        inputHarga: value,
        inputBerat: value
      })
    }
  
    handleDelete = (event) =>{
      let index = event.target.value;
      this.state.namaBuah.splice(index, 1)
      this.setState({namaBuah: this.state.namaBuah})
      this.state.hargaBuah.splice(index, 1)
      this.setState({hargaBuah: this.state.hargaBuah})
      this.state.beratBuah.splice(index, 1)
      this.setState({beratBuah: this.state.beratBuah})
    }
  
    render(){
      return(
        <div style={{margin: "0 auto", width: "50%"}}>
          <h1 style={{textAlign: 'center'}}>Daftar Buah</h1>
          <table style={{"border-collapse": "collapse",  border: "1px solid black", margin: "0 auto", padding: "30px"}}>
            <thead>
              <tr style={{backgroundColor: "grey"}}>
                <th style={{ border: "1px solid black", width: "200px"}}>No</th>
                <th style={{ border: "1px solid black", width: "200px"}}>Nama</th>
                <th style={{ border: "1px solid black", width: "200px"}}>Harga</th>
                <th style={{ border: "1px solid black", width: "200px"}}>Berat</th>
                <th style={{ border: "1px solid black", width: "200px"}}>Aksi</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor:"pink"}}>
                {
                  this.state.map((namaBuah, hargaBuah, beratBuah, index)=>{
                      var namaBuah = this.state.namaBuah;
                      var hargaBuah = this.state.hargaBuah;
                      var beratBuah = this.state.beratBuah;
                    var number = index+1; 
                    return(                    
                      <tr key={number}>
                        <td style={{ border: "1px solid black"}}>{number}</td>
                        <td style={{ border: "1px solid black"}}>{namaBuah}</td>
                        <td style={{ border: "1px solid black"}}>{hargaBuah}</td>
                        <td style={{ border: "1px solid black"}}>{beratBuah}</td>
                        <td style={{ border: "1px solid black"}}>
                          <button  value={index} onClick={this.handleEdit}>Edit</button>
                          <button style={{marginLeft: "1em"}} value={index} onClick={this.handleDelete}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
          <br/>
          <form style={{textAlign: "center"}} onSubmit={this.handleSubmit.bind(this)}>
            <label>
              Masukkan Nama Buah:
            </label>   
            <input type="text" required onChange={this.handleChange} value={this.state.inputBuah} />
            <br></br>
            <label>
              Masukkan Harga Buah:
            </label>          
            <input type="text" required onChange={this.handleChange} value={this.state.inputHarga} />
            <br></br>
            <label>
              Masukkan Berat Buah:
            </label>          
            <input type="text" required onChange={this.handleChange} value={this.state.inputBerat} />
            <br></br>
            <input type="submit" value="Submit" />
          </form>
        </div>
      )
    }
  }
  
  export default tugas12
  